﻿using UnityEngine;
using System.Collections;

namespace AugmentedReality
{
    public class EnemyController : MonoBehaviour
    {

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public void ValueChanged(float value)
        {
            gameObject.transform.localScale = Vector3.one * value;
            gameObject.transform.position = new Vector3(0, 0, value*2);
        }
    }
}
