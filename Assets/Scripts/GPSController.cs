﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace AugmentedReality
{
    public static class GPS
    {
        private static bool _init = false;
        public static bool Init
        {
            get
            {
                return _init;
            }
            set
            {
                _init = value;
                if (_init == true)
                    OnGPSInitDone(CurrentLocation);
            }
        }
        public static GPSLocation CurrentLocation = new GPSLocation();
        public static float Accuracy = 1f;
        public static float UpdateDistance = 1f;
        public delegate void GPSInitDone(GPSLocation cameraLocation);
        public static event GPSInitDone OnGPSInitDone;
    }

    public class GPSController : MonoBehaviour
    {

        public GameObject GPSInitPanel;

        IEnumerator Start()
        {
            Input.location.Start(GPS.Accuracy, GPS.UpdateDistance);
#if !UNITY_EDITOR
            while (Input.location.status != LocationServiceStatus.Running)
                yield return null;
#endif

            yield return new WaitForSeconds(5f);
            GPS.Init = true;
            GPS.CurrentLocation.SetPosition(Input.location.lastData);
            GPSInitPanel.SetActive(false);
            yield return null;
        }

        // Update is called once per frame
        void Update()
        {
            GPS.CurrentLocation.SetPosition(Input.location.lastData);
            //todo: change accuracy depending on distance to target
        }
    }
}