﻿using UnityEngine;
using System.Collections;

public class TargetArrowController : MonoBehaviour
{
    public static Transform targetGameobject = null;

    void Update()
    {
        if (targetGameobject == null) return;
        transform.position = Camera.main.transform.position;
        Vector3 lookAt = targetGameobject.position - transform.position;
        lookAt.y = 0;
        Quaternion lookAtRotation = Quaternion.LookRotation(lookAt);
        transform.rotation = lookAtRotation;
    }
}
