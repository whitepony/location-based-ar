﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SliderText : MonoBehaviour {

    public Text text;
    public Slider slider;
	
	void Update () {
        text.text = slider.value.ToString("f6");
	}
}
