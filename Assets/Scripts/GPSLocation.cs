﻿using UnityEngine;
using System;
using System.Collections;

[System.Serializable]
public class GPSLocation
{
    public double Latitude;
    public double Longitude;

    public GPSLocation(GPSLocation loc)
    {
        this.Latitude = loc.Latitude;
        this.Longitude = loc.Longitude;
    }

    public GPSLocation()
    {

    }

    private static double DegreesToRadians(double degrees)
    {
        return degrees * Math.PI / 180.0f;
    }

    private static double RadiansToDegrees(double angle)
    {
        return 180.0f * angle / Math.PI;
    }

    public double CalculateDistanceTo(GPSLocation location)
    {
        double circumference = 40000.0f; // Earth's circumference at the equator in km
        double distance = 0.0f;

        //Calculate radians
        double latitude1Rad = DegreesToRadians(this.Latitude);
        double longitude1Rad = DegreesToRadians(this.Longitude);
        double latititude2Rad = DegreesToRadians(location.Latitude);
        double longitude2Rad = DegreesToRadians(location.Longitude);

        double logitudeDiff = Math.Abs((longitude1Rad - longitude2Rad));

        if (logitudeDiff > Math.PI)
        {
            logitudeDiff = 2.0f * Math.PI - logitudeDiff;
        }

        double angleCalculation =
            Math.Acos(
              Math.Sin(latititude2Rad) * Math.Sin(latitude1Rad) +
              Math.Cos(latititude2Rad) * Math.Cos(latitude1Rad) * Math.Cos(logitudeDiff));

        distance = circumference * angleCalculation / (2.0f * Math.PI);

        return distance*1000.0; //to meters
    }
   public double CalculateBearing(GPSLocation location)
   {
      double lat1 = DegreesToRadians(this.Latitude);
      double lat2 = DegreesToRadians(location.Latitude);
      double dLon = DegreesToRadians(location.Longitude - this.Longitude);

      double dPhi = Math.Log(Math.Tan(lat2 / 2 + Math.PI / 4) / Math.Tan(lat1 / 2 + Math.PI / 4));
      if (Math.Abs(dLon) > Math.PI) dLon = (dLon > 0) ? -(2 * Math.PI - dLon) : (2 * Math.PI + dLon);
      double brng = Math.Atan2(dLon, dPhi);
 
      return (RadiansToDegrees(brng) + 360) % 360;
   }

    public static double CalculateDistance(params GPSLocation[] locations)
    {
        double totalDistance = 0.0f;

        for (int i = 0; i < locations.Length - 1; i++)
        {
            GPSLocation current = locations[i];
            GPSLocation next = locations[i + 1];

            totalDistance += current.CalculateDistanceTo(next);
        }

        return totalDistance;
    }

    public void SetPosition(LocationInfo locationInfo)
    {
        this.Latitude = locationInfo.latitude;
        this.Longitude = locationInfo.longitude;
    }

    public override string ToString()
    {
        return "(" + this.Latitude.ToString("f6") + " / " + this.Longitude.ToString("f6") + ")";
    }

    public static bool operator ==(GPSLocation a, GPSLocation b)
    {
        return a.Longitude == b.Longitude && a.Latitude == b.Latitude;
    }
    public static bool operator !=(GPSLocation a, GPSLocation b)
    {
        return a.Longitude != b.Longitude || a.Latitude != b.Latitude;
    }
}
