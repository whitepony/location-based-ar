﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace AugmentedReality
{
    public class DeviceCameraController : MonoBehaviour
    {
        
        void Start()
        {
#if UNITY_EDITOR
            gameObject.SetActive(false);
#else
            WebCamTexture webCamTexture = new WebCamTexture();
            webCamTexture.Play();
            GetComponent<RawImage>().texture = webCamTexture;
#endif
        }

    }
}