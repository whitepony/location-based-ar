﻿using UnityEngine;
using System.Collections;

namespace AugmentedReality
{
    public class GPSTracker : MonoBehaviour
    {
        public GPSLocation Location;

        void Start()
        {
#if !UNITY_EDITOR
            GPS.OnGPSInitDone += Spawn;
#endif
        }

        void Spawn(GPSLocation cameraLocation)
        {
            double GPSdiff = cameraLocation.CalculateDistanceTo(Location);
            double GPSdirection = cameraLocation.CalculateBearing(Location);
            transform.position = 
                new Vector3(Camera.main.transform.position.x, 0, Camera.main.transform.position.z) + 
                Quaternion.Euler(0, (float)GPSdirection, 0) * (Vector3.forward * (float)GPSdiff);
        }

        void Update()
        {

        }

    }
}
