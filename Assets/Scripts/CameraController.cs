﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Linq;

namespace AugmentedReality
{
    public class CameraController : MonoBehaviour
    {

        public Text debugText;
        public Slider enemySlider;
        public Slider lowPassFilter;
        public Transform[] enemyTarget;
        private Transform closestEnemy;
        public GameObject enemyPrefab;
        public GameObject bearingArrow;



        [Range(0,360)]
        public float DebugGPSBearing;
        [Range(0,10)]
        public float DebugGPSDiff;
        public GameObject DebugGPSBearingIndicator;
        private Vector3 initPosition;

        #region [Private fields]

        private Quaternion cameraBase = Quaternion.identity;
        private Quaternion calibration = Quaternion.identity;
        private Quaternion baseOrientation = Quaternion.Euler(90, 0, 0);

        private Quaternion referanceRotation = Quaternion.identity;
        private Quaternion lastRotationValue = Quaternion.identity;

        private Plane referenceAccelerationPlane;

        private KalmanFilter kalmanX;
        private KalmanFilter kalmanY;
        private KalmanFilter kalmanZ;

        private GPSLocation lastGPSPos = new GPSLocation();
        private GPSLocation currentGPSPos = new GPSLocation();

        private Vector3 nextCameraPositionInWorldSpace;

        private double GPSdiffText = 0;
        private double GPSdirText = 0;
        private bool posChanged = false;

        #endregion

        void Start()
        {
            Input.gyro.enabled = true;
            Screen.sleepTimeout = SleepTimeout.NeverSleep;
            initPosition = nextCameraPositionInWorldSpace = transform.position;

            /*
            ResetBaseOrientation();
            UpdateCalibration(true);
            UpdateCameraBaseRotation(true);
            RecalculateReferenceRotation();

            referenceAccelerationPlane = new Plane(Vector3.up, Vector3.zero); //XZ-Plane

            kalmanX = new KalmanFilter();
            kalmanY = new KalmanFilter();
            kalmanZ = new KalmanFilter();
            SetNoiseVarianceForKalman();
            */
            SensorHelper.ActivateRotation();
            GPS.OnGPSInitDone += InitCameraPosition;
        }

        void InitCameraPosition(GPSLocation location)
        {
            currentGPSPos = new GPSLocation(GPS.CurrentLocation);
            lastGPSPos = new GPSLocation(GPS.CurrentLocation);
        }

        void Update()
        {
            if (!GPS.Init) return;
            //Quaternion nextRotation = GetNewKalmanRotation();
            Quaternion nextRotation = SensorHelper.rotation;


            Quaternion nextRotationValue = nextRotation;//cameraBase * (ConvertRotation(referanceRotation * nextRotation));

            transform.rotation = lastRotationValue = nextRotationValue;

            //Y Angle between enemy and camera rotation
            /*
            if (enemyTarget != null)
            {
                float angle = Vector3.Angle(transform.forward, enemyTarget.position - transform.position);
                enemySlider.value = (angle / 180f);
            }
            */

            double GPSdiff = 0;
            double GPSdirection = 0;
            //get gps diff and direction to move
#if UNITY_EDITOR
            GPSdiff = DebugGPSDiff;
            GPSdirection = DebugGPSBearing;
            DebugGPSBearingIndicator.transform.rotation = Quaternion.Euler(0, (float)GPSdirection, 0);
            DebugGPSBearingIndicator.transform.position = Camera.main.transform.position;
#else
            currentGPSPos = new GPSLocation(GPS.CurrentLocation);
            if (currentGPSPos != lastGPSPos)
            {
                GPSdiff = GPSdiffText = lastGPSPos.CalculateDistanceTo(currentGPSPos);
                GPSdirection = GPSdirText = lastGPSPos.CalculateBearing(currentGPSPos);
                posChanged = true;
            }
            lastGPSPos = new GPSLocation(GPS.CurrentLocation);
#endif
            //set next camera position
            if (GPSdiff > 0)
            {
                nextCameraPositionInWorldSpace += Quaternion.Euler(0, (float)GPSdirection, 0) * (Vector3.forward * (float)GPSdiff);
            }
            transform.position = Vector3.Slerp(transform.position, nextCameraPositionInWorldSpace, Time.deltaTime);
            bearingArrow.transform.position = transform.position;
            bearingArrow.transform.rotation = Quaternion.Euler(0, (float)GPSdirText, 0);
            //float currentXAngle = Vector3.Angle(Input.acceleration.normalized, Vector3.right);
            //float currentZAngle = Vector3.Angle(Input.acceleration.normalized, Vector3.down);
            closestEnemy = enemyTarget.Aggregate((a, b) => Vector3.Distance(transform.position, a.position) < Vector3.Distance(transform.position, b.position) ? a : b);
            TargetArrowController.targetGameobject = closestEnemy;
            debugText.text =
                        "GPS: " + currentGPSPos.ToString() + " last: " + lastGPSPos.ToString() + ")" + "\n" +
                        "Diff: " + GPSdiffText.ToString("f9") + " / cur: " + GPSdiff.ToString("f9") + "\n" +
                        "Dir: " + GPSdirText.ToString("f9") + " / cur: " + GPSdirection.ToString("f9") + "\n" +
                        "Pos: " + transform.position.ToString("f3") + "\n" +
                        "Pos Enemy: " + closestEnemy.position.ToString("f3") + "\n" +
                        "GPS Dist: " + currentGPSPos.CalculateDistanceTo(closestEnemy.GetComponent<GPSTracker>().Location).ToString("f6") + "\n" +
                        "Dist: " + Vector3.Distance(transform.position, closestEnemy.position).ToString("f6") + "\n" +
                //        "Gyro: " + Input.gyro.attitude.eulerAngles.ToString("f6") + "\n" +
                //        "Gyro Rot UB: " + Input.gyro.rotationRateUnbiased.ToString("f6") + "\n" +
                //        "Rotation: " + SensorHelper.rotation.eulerAngles.ToString("f6") + "\n" +
                //        "Kalman: (" + kalmanX.ToString() + "," + kalmanY.ToString() + "," + kalmanZ.ToString() +  ")\n" +
                //"Accel: " + Input.acceleration.ToString("f6") + "\n" +
                //"Accel Rot: " + new Vector2(currentXAngle,currentZAngle).ToString("f6") + "\n" +
                "";
        }

        public void ResetToOriginalPosition()
        {
            transform.position = initPosition;
        }

        public void AdjustQAngle(float value)
        {
            kalmanX.setQangle(value);
            kalmanY.setQangle(value);
            kalmanZ.setQangle(value);
        }

        public void AdjustQBias(float value)
        {
            kalmanX.setQbias(value);
            kalmanY.setQbias(value);
            kalmanZ.setQbias(value);
        }

        public void AdjustRMeasure(float value)
        {
            kalmanX.setRmeasure(value);
            kalmanY.setRmeasure(value);
            kalmanZ.setRmeasure(value);
        }


        #region [Private methods]

        private void SetNoiseVarianceForKalman()
        {

        }

        private Quaternion GetNewKalmanRotation()
        {
            Quaternion rotation = SensorHelper.rotation;
            float xAngle = rotation.eulerAngles.x;
            float xRate = Input.gyro.rotationRateUnbiased.x;
            float newX = kalmanX.getAngle(xAngle, xRate, Time.fixedDeltaTime);


            float yAngle = rotation.eulerAngles.y;
            float yRate = Input.gyro.rotationRateUnbiased.z;
            float newY = kalmanY.getAngle(yAngle, yRate, Time.fixedDeltaTime);

            float zAngle = rotation.eulerAngles.z;
            float zRate = Input.gyro.rotationRateUnbiased.y;
            float newZ = kalmanZ.getAngle(zAngle, zRate, Time.fixedDeltaTime);

            return Quaternion.Euler((float)newX, (float)newY, (float)newZ);


        }
        /// <summary>
        /// Update the gyro calibration.
        /// </summary>
        private void UpdateCalibration(bool onlyHorizontal)
        {
            if (onlyHorizontal)
            {
                Vector3 fw = (Input.gyro.attitude) * (-Vector3.forward);
                fw.z = 0;
                if (fw == Vector3.zero)
                {
                    //Align to magnetic north pole
                    calibration = Quaternion.Euler(0, -Input.compass.magneticHeading, 0);
                }
                else
                {
                    calibration = (Quaternion.FromToRotation(Vector3.up, fw));
                    //Align to magnetic north pole
                    calibration.eulerAngles -= new Vector3(0, -Input.compass.magneticHeading, 0);
                }
            }
            else
            {
                calibration = Input.gyro.attitude;
            }
        }

        /// <summary>
        /// Update the camera base rotation.
        /// </summary>
        /// <param name='onlyHorizontal'>
        /// Only y rotation.
        /// </param>
        private void UpdateCameraBaseRotation(bool onlyHorizontal)
        {
            if (onlyHorizontal)
            {
                Vector3 fw = transform.forward;
                fw.y = 0;
                if (fw == Vector3.zero)
                {
                    cameraBase = Quaternion.identity;
                }
                else
                {
                    cameraBase = Quaternion.FromToRotation(Vector3.forward, fw);
                }
            }
            else
            {
                cameraBase = transform.rotation;
            }
        }

        /// <summary>
        /// Converts the rotation from right handed to left handed.
        /// </summary>
        /// <returns>
        /// The result rotation.
        /// </returns>
        /// <param name='q'>
        /// The rotation to convert.
        /// </param>
        private static Quaternion ConvertRotation(Quaternion q)
        {
            return new Quaternion(q.x, q.y, -q.z, -q.w);
        }

        /// <summary>
        /// Recalculates reference system.
        /// </summary>
        private void ResetBaseOrientation()
        {
            baseOrientation = Quaternion.Euler(90, 0, 0);
        }

        /// <summary>
        /// Recalculates reference rotation.
        /// </summary>
        private void RecalculateReferenceRotation()
        {
            referanceRotation = Quaternion.Inverse(baseOrientation) * Quaternion.Inverse(calibration);
        }

        #endregion
    }
}